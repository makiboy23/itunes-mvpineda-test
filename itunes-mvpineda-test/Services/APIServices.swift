//
//  APIServices.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import Foundation

class APIService {
    
    let iTunesAPIBaseUrl = "https://itunes.apple.com"
    let iTunesFeedBaseUrl = "https://rss.itunes.apple.com"
    
    static let shared = APIService()
    
    /**
        Load Top Music from itunes feed API
        source: https://rss.itunes.apple.com/api/v1/us/itunes-music/top-songs/all/10/explicit.json
        method: GET
    */
    func fetchTopMusicData(completion: @escaping ([Track]) -> ()) {
        let urlString = "\(iTunesFeedBaseUrl)/api/v1/us/itunes-music/top-songs/all/10/explicit.json"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                print("Failed to fetch songs:", err)
                return
            }
            
            guard let data = data else { return }
            do {
                var trackIDs = ""
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any],
                    let feed = json["feed"] as? [String:Any],
                    let results = feed["results"] as? [[String:Any]] {
                    
                    // DO LOOP the results
                    for dict in results as [[String: AnyObject]] {
                        
                        // MUST CHECK FIRST THE KIND
                        guard let kind = dict["kind"] as? String else { continue }
                        
                        if kind != "song" {
                            // IF NOT SONG THEN SKIP THE CURRENT LOOP TO CONTINUE
                            continue
                        }
                        
                        if let songId = dict["id"] {
                            // GET THE TRACKIDS STRING
                            trackIDs = "\(songId),\(trackIDs)"
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    self.fetchTracksData(trackIDs: trackIDs) { (tracks: [Track])  in
                       completion(tracks)
                    }
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
        }.resume()
    }
    
    /**
        Load Hot Tracks from itunes feed API
        source: https://rss.itunes.apple.com/api/v1/us/itunes-music/hot-tracks/all/10/explicit.json
        method: GET
     */
    func fetchHotTracksData(completion: @escaping ([Track]) -> ()) {
        let urlString = "\(iTunesFeedBaseUrl)/api/v1/us/itunes-music/hot-tracks/all/10/explicit.json"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                print("Failed to fetch songs:", err)
                return
            }
            
            guard let data = data else { return }
            do {
                var trackIDs = ""
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any],
                    let feed = json["feed"] as? [String:Any],
                    let results = feed["results"] as? [[String:Any]] {
                    
                    // DO LOOP the results
                    for dict in results as [[String: AnyObject]] {
                        
                        // MUST CHECK FIRST THE KIND
                        guard let kind = dict["kind"] as? String else { continue }
                        
                        if kind != "song" {
                            // IF NOT SONG THEN SKIP THE CURRENT LOOP TO CONTINUE
                            continue
                        }
                        
                        if let songId = dict["id"] {
                            // GET THE TRACKIDS STRING
                            trackIDs = "\(songId),\(trackIDs)"
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    self.fetchTracksData(trackIDs: trackIDs) { (tracks: [Track])  in
                        completion(tracks)
                    }
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
            }.resume()
    }
    
    /**
     Load New Music from itunes feed API
     source: https://rss.itunes.apple.com/api/v1/us/itunes-music/new-music/all/10/explicit.json
     method: GET
     return: Music Albums
     */
    func fetchNewMusicData(completion: @escaping ([Collection]) -> ()) {
        let urlString = "\(iTunesFeedBaseUrl)/api/v1/us/itunes-music/new-music/all/10/explicit.json"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                print("Failed to fetch music albums:", err)
                return
            }
            
            guard let data = data else { return }
            do {
                var albumIDs = ""
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any],
                    let feed = json["feed"] as? [String:Any],
                    let results = feed["results"] as? [[String:Any]] {
                    
                    // DO LOOP the results
                    for dict in results as [[String: AnyObject]] {
                        
                        // MUST CHECK FIRST THE KIND
                        guard let kind = dict["kind"] as? String else { continue }
                        
                        if kind != "album" {
                            // IF NOT ALBUM THEN SKIP THE CURRENT LOOP TO CONTINUE
                            continue
                        }
                        
                        if let albumId = dict["id"] {
                            // GET THE albumIDs STRING
                            albumIDs = "\(albumId),\(albumIDs)"
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    self.fetchAlbumData(albumIDs: albumIDs) { (collections: [Collection])  in
                        completion(collections)
                    }
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
        }.resume()
    }
    
    /**
     Load New Music from itunes feed API
     source: https://rss.itunes.apple.com/api/v1/us/itunes-music/new-music/all/10/explicit.json
     method: GET
     return: Track Collections
     */
    private func fetchAlbumData(albumIDs: String, completion: @escaping ([Collection]) -> ()) {
        let urlString = "\(iTunesAPIBaseUrl)/lookup?id=\(albumIDs)&entity=song"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                print("Failed to fetch music albums:", err)
                return
            }
            
            guard let data = data else { return }
            do {
                
                var collections = [Collection]()
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any],
                    let results = json["results"] as? [[String:Any]] {
                    
                    // DO LOOP the results
                    for dict in results as [[String: AnyObject]] {
              
                        // MUST CHECK FIRST THE WRAPPER TYPE
                        guard let wrapperType = dict["wrapperType"] as? String else { continue }
                        
                        if wrapperType == "collection" {
                            // do collection parsing
                            let parseCollectionItem = parseCollection(dict: dict)
                            
                            collections.append(parseCollectionItem)
                        } else if wrapperType == "track" {
                            // do track parsing
                            // find collection with collection id
                            
                            if let collectionId = dict["collectionId"] as? Int {
                                
                                // find Offset of collection
                                if let offset = collections.firstIndex(where: {$0.collectionId == collectionId}) {
                                    // do something with fooOffset
                                    
                                    let track = parseTrack(dict: dict)
                                    collections[offset].tracks.append(track)
                                }
                            }
                        }
                        
                    }
                    
                }
                
                DispatchQueue.main.async {
                    completion(collections)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
        }.resume()
    }
    
    /**
     Load Tracks from iTUNES Search API
     link: https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/
     source: https://itunes.apple.com/lookup?id={trackIds}
     method: GET
     
     - parameter trackIDs: The concatinated string trackId.
     */
    private func fetchTracksData(trackIDs: String, completion: @escaping ([Track]) -> ()) {
        let urlString = "\(iTunesAPIBaseUrl)/lookup?id=\(trackIDs)"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                print("Failed to fetch tracks:", err)
                return
            }
            
            guard let data = data else { return }
            do {
                // let tracks = try JSONDecoder().decode([Track].self, from: data)
                
                var tracks = [Track]()
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any],
                    let results = json["results"] as? [[String:Any]] {
                    
                    // DO LOOP the results
                    for dict in results as [[String: AnyObject]] {
                        var track = Track()
                        
                        // MUST CHECK FIRST THE WRAPPER TYPE
                        guard let wrapperType = dict["wrapperType"] as? String else { continue }
                        
                        if wrapperType != "track" {
                            // IF NOT TRACK THEN SKIP THE CURRENT LOOP TO CONTINUE
                            continue
                        }
                        
                        track = parseTrack(dict: dict)
                        
                        tracks.append(track)
                    }
                }
                
                DispatchQueue.main.async {
                    completion(tracks)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
        }.resume()
    }
}














