//
//  TopMusicCell.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/16/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import UIKit

class TopMusicCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    var homeControllerProtocol: HomeControllerProtocol?
    
    var tracks = [Track]() {
        didSet {
            // IF DATA IS LOADED THEN RELOAD THE PAGE
            collectionView.reloadData()
        }
    }
    
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "TOP MUSIC"
        lbl.font = UIFont.boldSystemFont(ofSize: 26)
        lbl.textColor = .orange
        lbl.textAlignment = .right
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        cv.isPagingEnabled = true
        
        return cv
    }()
    
    let cellId = "cellId"
    let cellTopMusicItemId = "cellTopMusicItemId"
    
    private func setupViews() {
        addSubview(collectionView)
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(TopMusicItemCell.self, forCellWithReuseIdentifier: cellTopMusicItemId)
        collectionView.topAnchor.constraint(equalTo: topAnchor, constant: 40).isActive = true
        collectionView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        collectionView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        addSubview(titleLabel)
        titleLabel.bottomAnchor.constraint(equalTo: collectionView.topAnchor, constant: -5).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: COLLECTIONVIEW
extension TopMusicCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let track = tracks[indexPath.row]
        homeControllerProtocol?.navigateToTrackController(track: track)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tracks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellTopMusicItemId, for: indexPath) as! TopMusicItemCell
        cell.track = tracks[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: frame.height)
    }
}
