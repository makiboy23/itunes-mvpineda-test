//
//  TopMusicItemCell.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/16/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import UIKit

class TopMusicItemCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    var track: Track? {
        didSet {
            if let artworkUrl600 = track?.artworkUrl600,
                let artworkUrl100 = track?.artworkUrl100 {
                
                // load image
                artWorkImageView.loadImage(urlString: artworkUrl600) {
                    // check if image is nil
                    if self.artWorkImageView.image == nil {
                        self.artWorkImageView.loadImage(urlString: artworkUrl100) {
                            if self.artWorkImageView.image == nil {
                                self.artWorkImageView.image = UIImage(named: "placeholder_image")
                            }
                        }
                    }
                }
            }
            
            if let trackName = track?.trackName {
                trackNameLabel.text = trackName
            }
            
            if let artistName = track?.artistName {
                artistNameLabel.text = artistName
            }
            
            if let trackPrice = track?.trackPrice,
                let currency = track?.currency {
                priceLabel.text = "\(trackPrice) \(currency)"
            }
            
            if let genre = track?.genre {
                genreLabel.text = genre
            }
        }
    }
    
    let itemView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 8
        view.layer.masksToBounds = true
        return view
    }()
    
    let artWorkShadowImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.image = UIImage(named: "placeholder_image")
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let artWorkImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let trackNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 18)
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let artistNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = .lightGray
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let priceLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textColor = .orange
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let genreLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let genreBadgeView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 31/255, green: 32/255, blue: 34/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 6
        view.layer.masksToBounds = true
        return view
    }()
    
    private func setupViews() {
        let percentageWidth = frame.width * 0.9
        let percentageMarginTop = (frame.width * 0.1) + 20
        
        addSubview(itemView)
        itemView.widthAnchor.constraint(equalToConstant: percentageWidth).isActive = true
        itemView.heightAnchor.constraint(equalTo: itemView.widthAnchor, multiplier: 1).isActive = true
        itemView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        itemView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -percentageMarginTop).isActive = true
        
        addSubview(artWorkImageView)
        artWorkImageView.topAnchor.constraint(equalTo: itemView.topAnchor).isActive = true
        artWorkImageView.leftAnchor.constraint(equalTo: itemView.leftAnchor).isActive = true
        artWorkImageView.rightAnchor.constraint(equalTo: itemView.rightAnchor).isActive = true
        artWorkImageView.bottomAnchor.constraint(equalTo: itemView.bottomAnchor).isActive = true
        
        addSubview(trackNameLabel)
        trackNameLabel.topAnchor.constraint(equalTo: itemView.bottomAnchor, constant: 10).isActive = true
        trackNameLabel.leftAnchor.constraint(equalTo: itemView.leftAnchor).isActive = true
        trackNameLabel.rightAnchor.constraint(equalTo: itemView.rightAnchor).isActive = true
        
        addSubview(artistNameLabel)
        artistNameLabel.topAnchor.constraint(equalTo: trackNameLabel.bottomAnchor).isActive = true
        artistNameLabel.leftAnchor.constraint(equalTo: trackNameLabel.leftAnchor).isActive = true
        artistNameLabel.rightAnchor.constraint(equalTo: trackNameLabel.rightAnchor).isActive = true
        
        addSubview(priceLabel)
        priceLabel.topAnchor.constraint(equalTo: artistNameLabel.bottomAnchor, constant: 8).isActive = true
        priceLabel.leftAnchor.constraint(equalTo: artistNameLabel.leftAnchor).isActive = true
        priceLabel.rightAnchor.constraint(equalTo: artistNameLabel.rightAnchor).isActive = true
        
        addSubview(genreLabel)
        genreLabel.topAnchor.constraint(equalTo: itemView.topAnchor, constant: 20).isActive = true
        genreLabel.rightAnchor.constraint(equalTo: itemView.rightAnchor, constant: -20).isActive = true
        
        insertSubview(genreBadgeView, belowSubview: genreLabel)
        genreBadgeView.topAnchor.constraint(equalTo: genreLabel.topAnchor, constant: -10).isActive = true
        genreBadgeView.leftAnchor.constraint(equalTo: genreLabel.leftAnchor, constant: -10).isActive = true
        genreBadgeView.rightAnchor.constraint(equalTo: genreLabel.rightAnchor, constant: 10).isActive = true
        genreBadgeView.bottomAnchor.constraint(equalTo: genreLabel.bottomAnchor, constant: 10).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
