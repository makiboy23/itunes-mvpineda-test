//
//  HeaderListCell.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/16/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import UIKit

class HeaderListCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    var previouslyVisitedDateTime: String = "" {
        didSet {
            dateTitleLabel.text = previouslyVisitedDateTime
        }
    }
    
    let headerTitleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "iTunes Music"
        lbl.font = UIFont.italicSystemFont(ofSize: 26)
        lbl.textColor = .white
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let dateTitleLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 11)
        lbl.textColor = .white
        lbl.textAlignment = .right
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let dateTileTextLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Previous Date Time Visited"
        lbl.font = UIFont.systemFont(ofSize: 8)
        lbl.textColor = .white
        lbl.textAlignment = .right
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private func setupViews() {
        addSubview(headerTitleLabel)
        headerTitleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        headerTitleLabel.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor).isActive = true
        
        addSubview(dateTitleLabel)
        dateTitleLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        dateTitleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        
        addSubview(dateTileTextLabel)
        dateTileTextLabel.topAnchor.constraint(equalTo: dateTitleLabel.bottomAnchor).isActive = true
        dateTileTextLabel.rightAnchor.constraint(equalTo: dateTitleLabel.rightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
