//
//  NewMusicCell.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import UIKit

class NewMusicCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    var collections = [Collection]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    let titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "New Music"
        lbl.font = UIFont.boldSystemFont(ofSize: 26)
        lbl.textColor = .orange
        lbl.textAlignment = .right
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = .clear
        cv.delegate = self
        cv.dataSource = self
        cv.isPagingEnabled = true
        cv.isScrollEnabled = false
        
        return cv
    }()
    
    let cellId = "cellId"
    
    private func setupViews() {
        addSubview(titleLabel)
        titleLabel.topAnchor.constraint(equalTo: topAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        
        addSubview(collectionView)
        collectionView.register(NewMusicItemCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10).isActive = true
        collectionView.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        collectionView.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -20).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: COLLECTIONVIEW
extension NewMusicCell: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! NewMusicItemCell
        cell.collection = collections[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // New Music Box Height
        let newMusicBoxSpacingBetween: CGFloat = 10
        let newMusicRowMargin: CGFloat = 40
        let newMusicBoxHeight: CGFloat = (frame.width - (newMusicBoxSpacingBetween + newMusicRowMargin)) / 2
        
        // Calculate 2 boxes per row with 10 spacing between
        return CGSize(width: newMusicBoxHeight, height: newMusicBoxHeight)
    }
}
