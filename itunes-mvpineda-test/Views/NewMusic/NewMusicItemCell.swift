//
//  NewMusicItemCell.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import UIKit

class NewMusicItemCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    var collection: Collection? {
        didSet {
            if let artworkUrl600 = collection?.artworkUrl600,
                let artworkUrl100 = collection?.artworkUrl100 {
                
                // load image
                artWorkImageView.loadImage(urlString: artworkUrl600) {
                    // check if image is nil
                    self.artWorkImageView.loadImage(urlString: artworkUrl100) {
                        if self.artWorkImageView.image == nil {
                            self.artWorkImageView.image = UIImage(named: "placeholder_image")
                        }
                    }
                }
            }
            
            if let collectionName = collection?.collectionName {
                collectionNameLabel.text = collectionName
            }
            
            if let artistName = collection?.artistName {
                artistNameLabel.text = artistName
            }
            
            if let collectionPrice = collection?.collectionPrice,
                let currency = collection?.currency {
                priceLabel.text = "\(collectionPrice) \(currency)"
            }
            
            if let genre = collection?.genre {
                genreLabel.text = genre
            }
        }
    }
    
    let artWorkImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.image = UIImage(named: "placeholder_image")
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = .black
        iv.layer.masksToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let collectionNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = .white
        lbl.textAlignment = .left
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 2
        return lbl
    }()
    
    let artistNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 9)
        lbl.textColor = .lightGray
        lbl.textAlignment = .left
        lbl.numberOfLines = 2
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let priceLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 10)
        lbl.textColor = .orange
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let genreLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 9)
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let genreBadgeView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 31/255, green: 32/255, blue: 34/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 6
        view.layer.masksToBounds = true
        return view
    }()

    
    let detailsView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private func setupViews() {
        backgroundColor = .white
        layer.cornerRadius = 8
        layer.masksToBounds = true
        
        addSubview(artWorkImageView)
        artWorkImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        artWorkImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        artWorkImageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        artWorkImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        addSubview(priceLabel)
        priceLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        priceLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
        
        addSubview(artistNameLabel)
        artistNameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        artistNameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        artistNameLabel.bottomAnchor.constraint(equalTo: priceLabel.topAnchor, constant: -8).isActive = true
        
        addSubview(collectionNameLabel)
        collectionNameLabel.leftAnchor.constraint(equalTo: artistNameLabel.leftAnchor).isActive = true
        collectionNameLabel.rightAnchor.constraint(equalTo: artistNameLabel.rightAnchor).isActive = true
        collectionNameLabel.bottomAnchor.constraint(equalTo: artistNameLabel.topAnchor).isActive = true
        
        insertSubview(detailsView, belowSubview: priceLabel)
        detailsView.topAnchor.constraint(equalTo: collectionNameLabel.topAnchor, constant: -10).isActive = true
        detailsView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        detailsView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        detailsView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        addSubview(genreLabel)
        genreLabel.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        genreLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        
        insertSubview(genreBadgeView, belowSubview: genreLabel)
        genreBadgeView.topAnchor.constraint(equalTo: genreLabel.topAnchor, constant: -10).isActive = true
        genreBadgeView.leftAnchor.constraint(equalTo: genreLabel.leftAnchor, constant: -10).isActive = true
        genreBadgeView.rightAnchor.constraint(equalTo: genreLabel.rightAnchor, constant: 10).isActive = true
        genreBadgeView.bottomAnchor.constraint(equalTo: genreLabel.bottomAnchor, constant: 10).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
