//
//  HotTracksItemCell.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/16/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import UIKit

class HotTracksItemCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    var track: Track? {
        didSet {
            if let artworkUrl600 = track?.artworkUrl600 {
                
                // load image
                artWorkImageView.loadImage(urlString: artworkUrl600) {
                    // check if image is nil
                    if self.artWorkImageView.image == nil {
                        self.artWorkImageView.image = UIImage(named: "placeholder_image")
                    }
                }
            }
            
            if let trackName = track?.trackName {
                trackNameLabel.text = trackName
            }
            
            if let artistName = track?.artistName {
                artistNameLabel.text = artistName
            }
            
            if let trackPrice = track?.trackPrice,
                let currency = track?.currency {
                priceLabel.text = "\(trackPrice) \(currency)"
            }
            
            if let genre = track?.genre {
                genreLabel.text = genre
            }
        }
    }
    
    let artWorkImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.image = UIImage(named: "placeholder_image")
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = UIColor(red: 31/255, green: 32/255, blue: 34/255, alpha: 1)
        iv.layer.masksToBounds = true
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let trackNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 2
        return lbl
    }()
    
    let artistNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 8)
        lbl.textColor = .lightGray
        lbl.textAlignment = .center
        lbl.numberOfLines = 2
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let priceLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 10)
        lbl.textColor = .orange
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let genreLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = .black
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let contentWrapperView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private func setupViews() {
        backgroundColor = .white
        layer.cornerRadius = 8
        layer.masksToBounds = true
        
        // Calculate artwork aspect width
        let artWorkWidth = frame.width * 0.8 // 80%
        
        let contentWrapperHeight = artWorkWidth + 10 + 8 + 60
        
        addSubview(contentWrapperView)
        contentWrapperView.widthAnchor.constraint(equalToConstant: frame.width).isActive = true
        contentWrapperView.heightAnchor.constraint(equalToConstant: contentWrapperHeight).isActive = true
        contentWrapperView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        addSubview(artWorkImageView)
        artWorkImageView.topAnchor.constraint(equalTo: contentWrapperView.topAnchor).isActive = true
        artWorkImageView.widthAnchor.constraint(equalToConstant: artWorkWidth).isActive = true
        artWorkImageView.heightAnchor.constraint(equalTo: artWorkImageView.widthAnchor, multiplier: 1.0).isActive = true
        artWorkImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        artWorkImageView.layer.cornerRadius = artWorkWidth / 2
        
        addSubview(trackNameLabel)
        trackNameLabel.topAnchor.constraint(equalTo: artWorkImageView.bottomAnchor, constant: 10).isActive = true
        trackNameLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        trackNameLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        addSubview(artistNameLabel)
        artistNameLabel.topAnchor.constraint(equalTo: trackNameLabel.bottomAnchor).isActive = true
        artistNameLabel.leftAnchor.constraint(equalTo: trackNameLabel.leftAnchor).isActive = true
        artistNameLabel.rightAnchor.constraint(equalTo: trackNameLabel.rightAnchor).isActive = true
        
        addSubview(priceLabel)
        priceLabel.topAnchor.constraint(equalTo: artistNameLabel.bottomAnchor, constant: 8).isActive = true
        priceLabel.leftAnchor.constraint(equalTo: trackNameLabel.leftAnchor).isActive = true
        priceLabel.rightAnchor.constraint(equalTo: trackNameLabel.rightAnchor).isActive = true
        
        addSubview(genreLabel)
        genreLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        genreLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
