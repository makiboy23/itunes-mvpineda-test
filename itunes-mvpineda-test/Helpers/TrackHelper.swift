//
//  TrackHelper.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import Foundation

func parseTrack(dict: [String: AnyObject]) -> Track {
    let track = Track()
    
    if let artistId = dict["artistId"] {
        track.artistId = artistId as? Int
    }
    
    if let artistName = dict["artistName"] {
        track.artistName = artistName as? String
    }
    
    if let trackId = dict["trackId"] {
        track.trackId = trackId as? Int
    }
    
    if let trackName = dict["trackName"] {
        track.trackName = trackName as? String
    }
    
    if let trackPrice = dict["trackPrice"] {
        track.trackPrice = trackPrice as? Double
    }
    
    if let genre = dict["primaryGenreName"] {
        track.genre = genre as? String
    }
    
    if let currency = dict["currency"] {
        track.currency = currency as? String
    }
    
    if let artworkUrl600 = dict["artworkUrl100"] {
        // do some size hacking
        
        if let artworkUrlString = artworkUrl600 as? String {
            track.artworkUrl600 = artworkUrlString.replacingOccurrences(of: "100x100", with: "600x600")
        }
    }
    
    if let artworkUrl100 = dict["artworkUrl100"] {
        track.artworkUrl100 = artworkUrl100 as? String
    }
    
    if let artworkUrl60 = dict["artworkUrl60"] {
        track.artworkUrl60 = artworkUrl60 as? String
    }
    
    if let artworkUrl30 = dict["artworkUrl30"] {
        track.artworkUrl30 = artworkUrl30 as? String
    }
    
    if let longDescription = dict["longDescription"] {
        track.longDescription = longDescription as? String
    }
    
    return track
}
