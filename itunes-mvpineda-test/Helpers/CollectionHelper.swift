//
//  CollectionHelper.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import Foundation

func parseCollection(dict: [String: AnyObject]) -> Collection {
    let newCollection = Collection()
    
    if let artistId = dict["artistId"] as? Int {
        newCollection.artistId = artistId
    }
    
    if let artistName = dict["artistName"] as? String {
        newCollection.artistName = artistName
    }
    
    if let collectionId = dict["collectionId"] as? Int {
        newCollection.collectionId = collectionId
    }
    
    if let collectionName = dict["collectionName"] as? String {
        newCollection.collectionName = collectionName
    }
    
    if let collectionPrice = dict["collectionPrice"] as? Double {
        newCollection.collectionPrice = collectionPrice
    }
    
    if let currency = dict["currency"] as? String {
        newCollection.currency = currency
    }
    
    if let genre = dict["primaryGenreName"] {
        newCollection.genre = genre as? String
    }
    
    if let artworkUrl600 = dict["artworkUrl100"] {
        // do some size hacking
        
        if let artworkUrlString = artworkUrl600 as? String {
            newCollection.artworkUrl600 = artworkUrlString.replacingOccurrences(of: "100x100", with: "600x600")
        }
    }
    
    if let artworkUrl100 = dict["artworkUrl100"] {
        newCollection.artworkUrl100 = artworkUrl100 as? String
    }
    
    return newCollection
}

