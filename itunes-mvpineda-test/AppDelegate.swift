//
//  AppDelegate.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/16/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow()
        window?.makeKeyAndVisible()
        
        let layout = UICollectionViewFlowLayout()
        window?.rootViewController = HomeController(collectionViewLayout: layout)
        
        return true
    }
    
    // MARK:- DETECT APPLICATION TERMINATION
    func applicationWillTerminate(_ application: UIApplication) {
        // IF TERMINAL CHANGE THE LAST DATE TIME VISITED
        let _ = saveVisitedDateTime()
    }
}

