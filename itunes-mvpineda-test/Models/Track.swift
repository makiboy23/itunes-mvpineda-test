//
//  Track.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import Foundation

class Track: NSObject {
    var artistId: Int?
    var artistName: String?
    
    var trackId: Int?
    var trackName: String?
    var trackPrice: Double?
    var genre: String?
    
    var currency: String?
    
    var artworkUrl600: String?
    var artworkUrl100: String?
    var artworkUrl60: String?
    var artworkUrl30: String?
    
    var longDescription: String?
}
