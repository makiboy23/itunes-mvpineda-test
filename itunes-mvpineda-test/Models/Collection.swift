//
//  Album.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import Foundation

class Collection: NSObject {
    var artistId: Int?
    var artistName: String?
    
    var collectionId: Int?
    var collectionName: String?
    var collectionPrice: Double?
    var currency: String?
    var genre: String?
    
    var artworkUrl100: String?
    var artworkUrl600: String?
    
    var tracks:[Track] = [Track]()
}
