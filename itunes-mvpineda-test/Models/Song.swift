//
//  Feed.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import Foundation

class Song: NSObject {
    var songId: Int? // SONG ID = TRACK ID
    var songName: String? // SONG NAME = TRACKNAME
}
