//
//  ViewController.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/16/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import UIKit

protocol HomeControllerProtocol {
    func navigateToTrackController(track: Track)
}

class HomeController: UICollectionViewController, HomeControllerProtocol {
    override init(collectionViewLayout layout: UICollectionViewLayout) {
        super.init(collectionViewLayout: layout)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupObservers()
        fetchTopMusic()
        fetchHotTracks()
        fetchNewMusic()
    }
    
    var previouslyVisitedDateTime = ""
    
    var topMusicTracks = [Track]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var hotTracks = [Track]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var newMusic = [Collection]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    let cellId = "cellId"
    let cellHeaderId = "cellHeaderId"
    let cellTopMusicId = "cellTopMusicId"
    let cellHotTracksId = "cellHotTracksId"
    let cellNewMusicId = "cellNewMusicId"
    
    // MARK: SETUP VIEWS
    
    private func setupViews() {
        if #available(iOS 11.0, *) {
            collectionView?.contentInsetAdjustmentBehavior = .never
        }
        
        // FETCH PREVIOUSLY DATE TIME
        previouslyVisitedDateTime = fetchPreviouslyVisitedDateTime()
        
        collectionView.bounces = false
        collectionView.backgroundColor = UIColor(red: 31/255, green: 32/255, blue: 34/255, alpha: 1)
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(HeaderListCell.self, forCellWithReuseIdentifier: cellHeaderId)
        collectionView.register(TopMusicCell.self, forCellWithReuseIdentifier: cellTopMusicId)
        collectionView.register(HotTracksCell.self, forCellWithReuseIdentifier: cellHotTracksId)
        collectionView.register(NewMusicCell.self, forCellWithReuseIdentifier: cellNewMusicId)
        
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    private func setupObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(willResignActive), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MAKE THE STATUS TEXT WHITE
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK:- NAVIGATION
extension HomeController {
    func navigateToTrackController(track: Track) {
        let controller = TrackController()
        controller.track = track
        present(controller, animated: true)
    }
}

// MARK:- OBSERVERS
extension HomeController {
    @objc func willResignActive() {
        // TODO:- Do save/update current date and time when the app is resign active
        
        previouslyVisitedDateTime = saveVisitedDateTime()
        collectionView.reloadData()
    }
}

// MARK:- FETCH DATA
extension HomeController {
    private func fetchTopMusic() {
        APIService.shared.fetchTopMusicData { (tracks: [Track])  in
            self.topMusicTracks = tracks
        }
    }
    
    private func fetchHotTracks() {
        APIService.shared.fetchHotTracksData { (tracks: [Track])  in
            self.hotTracks = tracks
        }
    }
    
    private func fetchNewMusic() {
        APIService.shared.fetchNewMusicData { (collections: [Collection])  in
            self.newMusic = collections
        }
    }
}

// MARK: COLLECTIONVIEW
extension HomeController: UICollectionViewDelegateFlowLayout {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellHeaderId, for: indexPath) as! HeaderListCell
            cell.previouslyVisitedDateTime = previouslyVisitedDateTime
            return cell
        } else if indexPath.row == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellTopMusicId, for: indexPath) as! TopMusicCell
            cell.homeControllerProtocol = self
            cell.tracks = topMusicTracks
            return cell
        } else if indexPath.row == 2 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellHotTracksId, for: indexPath) as! HotTracksCell
            cell.homeControllerProtocol = self
            cell.tracks = hotTracks
            return cell
        }  else if indexPath.row == 3 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellNewMusicId, for: indexPath) as! NewMusicCell
            cell.collections = newMusic
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let rowSectionBottomSpacing: CGFloat = 40
        var rowSize: CGSize = .zero
        
        // to achieve RESPONSIVE DESIGN CALCULATION
        
        if indexPath.row == 0 {
            rowSize = CGSize(width: view.frame.width, height: 120)
            
        } else if indexPath.row == 1 {
            // 10%(add extra height) + 60(for pagination) height
            let topMusicDetailsHeight = (view.frame.width * 0.1) + 60 // 10%(add extra height) + 60(for pagination)
            
            /// Aspect Ratio 1:1
            let topMusicRowSize = CGSize(width: view.frame.width, height: view.frame.width + topMusicDetailsHeight + rowSectionBottomSpacing)
            rowSize = topMusicRowSize
            
        } else if indexPath.row == 2 {
            
            let hotTracksTitleHeight: CGFloat = 20
            
            // divide into 3 column and multiply by 2 = height
            let hotTrackBoxHeight = (view.frame.width / 3) * 2
            let hotTracksRowSize = CGSize(width: view.frame.width, height: hotTrackBoxHeight + hotTracksTitleHeight + rowSectionBottomSpacing)
            
            rowSize = hotTracksRowSize
        } else if indexPath.row == 3 {
            
            // New Music Box Height
            let newMusicTitleHeight: CGFloat = 20
            let newMusicBoxSpacingBetween: CGFloat = 10
            let newMusicRowMargin: CGFloat = 40
            
            // Calculate 2 boxes per row with 10 spacing between
            let newMusicBoxHeight: CGFloat = (view.frame.width - (newMusicBoxSpacingBetween + newMusicRowMargin)) / 2
            
            let newMusicRowCount: CGFloat = ceil(10 / 2)
            let newMusicRowSpacing: CGFloat = newMusicRowCount * 10
            
            let newMusicSectionHeight = (newMusicBoxHeight * newMusicRowCount + newMusicRowSpacing) + rowSectionBottomSpacing
            
            // 6x New Music = 6x Box
            let newMusicRowSize = CGSize(width: view.frame.width, height: newMusicSectionHeight + newMusicTitleHeight + 20)
            
            rowSize = newMusicRowSize
        }
        
        return rowSize
    }
}
